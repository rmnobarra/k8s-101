# k8s 101

1. who i am

2. intro

3. conceitos
    * o que é
    * arquitetura
    * componentes
    * objetos

4. demo
    * kubernetes saas em Amazon web services

5. bônus
    * iac
     - ansible

6. links e referências

---

# who i am

* +15 anos experiência
* network
* linux
* containers
* ansible
* terraform
* vagrant
* chef
* shell script
* python
* cloud pública / privada

# intro

Dificilmente uma empresa hoje em dia está fora do ciclo de construir, manter ou consumir sistemas. Informações rápidas e consistentes equivalem a ganhar ou perder dinheiro, e os dados possuem valores muitas vezes, incalculáveis. 

Por isso, equipes de desenvolvimento tem a infinita tarefa de entregar novos sistemas com maior frequência enquanto infraestrutra, estar preparado para atender essa demanda. A virtualização, sucedida pela conteinerização veio para resolver parte dessa demanda e obviamente, novas tecnologias trazem novos desafios.

# kubernetes

Orquestrar e gerenciar o ciclo de vida de aplicações conteinerizadas é falar de kubernetes, pois sua robustez, portabilidade e escalabilidade,
junto com um ecossistema extremamente ativo e em constante expansão, tornou-o padrão de mercado.

## o que é

O Kubernetes é uma plataforma portátil, extensível e de código aberto para o gerenciamento de cargas de trabalho e serviços em contêineres, que facilita a configuração declarativa e a automação.

O nome Kubernetes se origina do grego, significando timoneiro ou piloto. O Google abriu o código do projeto Kubernetes em 2014. O Kubernetes combina mais de 15 anos de experiência do Google executando cargas de trabalho de produção em escala com as melhores ideias e práticas da comunidade.

## arquitetura


![rquitetura k8s](img/arquitetura.png)


* control plane

Uma série de componentes que gerencia o cluster como um todo. Escalonamento em pods, reṕlicas em um deployment.

* kube-apiserver 
Frontend do control plane,

* etcd
Banco de dados chave / valor que armazena todos as informações do cluster. 

* kube-scheduler
É responsável por alocar um nó para determinado workload. Pode se basear em alguns críterios como, labels, afinidade, anti afinidade, cpu e memoria.

* kube-controller-manager
Responsável por executar os processos:
- node controller
- replication controller
- endpoints controllers
- service account e token controllers

* cloud-controller-manager
Responsável por fazer proxy com api's de cloud providers.

* kubelet

Agent instalado em cada nó. Responsável por executar o(s) containers dentro de um pod.

* kube-proxy
É executado em cada nó e é responsável pela comunicação dentro do cluster. Cria regras de firewall 
utilizando netfilter (iptables).

* Container runtime
responsável por executar os containers.

* kubectl
CLI do Kubernetes, kubectl, permite que você execute comandos em clusters. Você pode usar kubectl para implantar aplicativos, inspecionar e gerenciar recursos de cluster e ver logs.

## objetos

Entidades persistentes dentro de um cluster.

* namespace
* daemonset
* deployment
* services
* ingress
* pod

Esses objetos são aplicados no cluster basicamente de duas maneiras

* imperativa
* declarativa

Imperativa é a execução do comando "on the fly" usando kubectl
Declarativa utiliza arquivos .yaml para aplicar o estado desejável de determinado objeto dentro do cluster

## Pods

Os pods são as menores unidades de computação implantáveis que você pode criar e gerenciar no Kubernetes.

Um pod (como em um pod de baleias) é um grupo de um ou mais contêineres, com armazenamento / recursos de rede compartilhados e uma especificação de como executar os contêineres.

## Demo

O objetivo dessa demo é criar um cluster gerenciado na amazon e efetuar alguns deploys de objetos dentro do cluster.

Os prérequisitos são:

aws cli >= 2.1.1
ansible >= 2.9.6
terraform >= 12.28
aws-iam-authenticator >= 1.18.9	
kubectl >= 1.19.4
python >= 2.7
openshift >= 0.6
PyYAML >= 3.11



O link https://learn.hashicorp.com/tutorials/terraform/eks será utilizado como base.

1. clone do repo
```
git clone https://github.com/hashicorp/learn-terraform-provision-eks-cluster
```

2. acessando o dir
```
cd learn-terraform-provision-eks-cluster
```

3. satisfazendo dependencias
```
terraform init
```

4. aplicando o projeto terraform
```
terraform apply
```

5. configurando kubectl
```
aws eks --region $(terraform output region) update-kubeconfig --name $(terraform output cluster_name)
```

6. Dependências playbook
```
ansible-galaxy collection install community.kubernetes
```
6. executando playbook para criar namespace

```
ansible-playbook ansible/namespace.yaml
```

7. executando playbook para criar deployment
```
ansible-playbook ansible/deployment.yaml
```

8. Proxy para aplicação
```
kubectl port-forward deployment/kuard 8080:8080  -n kuard
```

9. acessando aplicação

http://localhost:8080/


## Referências:

https://learn.hashicorp.com/tutorials/terraform/eks

https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/